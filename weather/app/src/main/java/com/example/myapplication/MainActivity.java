package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        city=findViewById(R.id.cityInput);
        temp=findViewById(R.id.value);

    }
    EditText city;
    TextView temp;
    public  void City(){
        String url = "https://api.openweathermap.org/data/2.5/weather?q=" + city.getText().toString() + "&appid=b1b35bba8b434a28a0be2a3e1071ae5b&units=metric";
        JsonObjectRequest jo = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                   JSONObject main = response.getJSONObject("main");
                    String temp2 = String.valueOf(main.getInt("temp"));
                    temp.setText(temp2.toString());
                } catch (JSONException e) {temp.setText(e.getMessage()); } }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { }});
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jo);
    }
    public void click(View view) {
City();
    }
}
